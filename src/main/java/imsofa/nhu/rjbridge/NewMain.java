/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.nhu.rjbridge;

import java.util.List;
import java.util.Map;
import ognl.Ognl;

/**
 *
 * @author lendle
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        RJ rj=new RJ("/usr/bin/Rscript");
        rj.appendRCode("setwd('/home/lendle/Copy/工作中/stock')");
        rj.appendRCode("source('ud.r')");
        rj.appendRCode("a<-fullPredict(read.csv('1234.csv'))");
        rj.setReturnedObjects(new String[]{"a"});
        Map map=rj.run();
        
        Map a=(Map) map.get("a");
        List<String> bestCols=(List<String>) rj.getResultOGNL("a.lmBestCols");
        System.out.println("intercept="+rj.getResultOGNL("a.lmSummary.coefficients[0].Estimate"));
        int i=1;
        for(String col : bestCols){
            System.out.println(col+"="+rj.getResultOGNL("a.lmSummary.coefficients["+i+"].Estimate"));
            i++;
        }
    }
    
}
