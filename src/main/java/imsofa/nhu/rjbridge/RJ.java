/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.nhu.rjbridge;

import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import ognl.Ognl;
import ognl.OgnlException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 * RJ is the main interface class to bridge
 * java code to R executable
 * usage:
 * <ol>
 * <li>register the path rscript</li>
 * <li>setReturnedObjects</li>
 * <li>appendRCode</li>
 * <li>run</li>
 * <li>retrieve returned objects through the returned map or access returned objects via getResultOGNL</li>
 * </ol>
 * @author lendle
 */
public class RJ {
    private final String INIT_CODE="if(!require(RJSONIO)){cat('installing RJSONIO');\ninstall.packages('RJSONIO', repos='http://cran.us.r-project.org');}\nlibrary(RJSONIO);";
    private String rscriptPath = null;
    private String rCode = INIT_CODE;
    private Map result = null;
    private String[] returnedObjects = null;

    /**
     * initialize a RJ instance
     * @param rscriptPath a full path to the rscript executable (rscript on linux or rscript.exe on windows)
     */
    public RJ(String rscriptPath) {
        this.rscriptPath = rscriptPath;
    }

    public String getRscriptPath() {
        return rscriptPath;
    }

    public void setRscriptPath(String rscriptPath) {
        this.rscriptPath = rscriptPath;
    }

    public String getrCode() {
        return rCode;
    }

    public String[] getReturnedObjects() {
        return returnedObjects;
    }
    /**
     * set the name of objects to be returned
     * @param returnedObjects 
     */
    public void setReturnedObjects(String[] returnedObjects) {
        this.returnedObjects = returnedObjects;
    }

    public void appendRCode(String code) {
        rCode = rCode + "\n" + code;
    }

    public void clearRCode() {
        this.rCode = INIT_CODE;
    }
    /**
     * execute appended R codes
     * @return a map containing all the registered returnedObjects
     * @throws IOException
     * @throws InterruptedException
     * @throws RJException 
     */
    public Map run() throws IOException, InterruptedException, RJException {
        File resultFile = File.createTempFile("rjBridge", ".tmp.json");
        if (this.returnedObjects != null) {
            this.appendRCode("rjReturn<-list()");
            for (String obj : this.returnedObjects) {
                this.appendRCode("rjReturn$" + obj + "<-" + obj);
            }
            this.appendRCode("write(toJSON(rjReturn), '" + resultFile.getCanonicalPath().replace('\\', '/') + "')");
        }
        File tempScriptFile = File.createTempFile("rjBridge", ".tmp.R");
        FileUtils.write(tempScriptFile, this.rCode);
        ProcessBuilder pb = new ProcessBuilder(this.rscriptPath, tempScriptFile.getCanonicalPath());
        pb.redirectErrorStream(true);
        Process p = pb.start();
        int ret = p.waitFor();
        try {
            if(ret!=0){
                //error
                RJException e=new RJException(IOUtils.toString(p.getInputStream(), "utf-8"));
                throw e;
            }
            if (this.returnedObjects != null && this.returnedObjects.length > 0) {
                String str = FileUtils.readFileToString(resultFile);
                this.result = new Gson().fromJson(str, Map.class);
            }
        } finally {
            FileUtils.deleteQuietly(resultFile);
            FileUtils.deleteQuietly(tempScriptFile);
        }
        //System.out.println(IOUtils.toString(p.getInputStream(), "utf-8"));

        return this.result;
    }

    public Map getResult() {
        return result;
    }
    /**
     * access returned objects using OGNL notation e.g. a.lmBestCols
     * @param expression
     * @return
     * @throws OgnlException 
     */
    public Object getResultOGNL(String expression) throws OgnlException{
        return Ognl.getValue(expression, this.result);
    }
}
